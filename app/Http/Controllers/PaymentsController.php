<?php

namespace App\Http\Controllers;
use App\Voucher;
use App\Traits\ApiResponser\Success;
use App\Traits\ApiResponser\Error;
use Illuminate\Http\Request;
use App\Http\Resources\VoucherResource;
use Illuminate\Database\QueryException;

use MongoDB\Driver\Exception\BulkWriteException as MongoError;

use Carbon\Carbon;
class PaymentsController extends Controller
{
    use Success;
    use Error;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public  function index()
    {
        $cars =  Car::all();
        return $this->successResponse($cars);
    }

    public  function store(Request $request)
    {
        try {
            
                $start = $request['start_time'];
                $end = $request['end_time'];
                $totalHours =  Carbon::parse($end)->diffInSeconds(Carbon::parse($start)) / 60;
                $amount = $totalHours * env('PRICE_PER_MINUTE');
                switch(true) {
                    case $request['type_of_car'] === 'official':
                        $data = [
                                'time' => $totalHours,
                                'car_id' => $request->car_id,
                                'amount' => 0];
                    break;
                    case $request['type_of_car'] === 'resident':    
                        $amountMonthlyAmount = $this->getMonthlyAmount($request['car_id']);

                        $data = [
                                'time' => $totalHours ,
                                'car_id' => $request->car_id,
                                'amount' => ($amount +  $amountMonthlyAmount)
                            ];
                    break;
            
                }

                $payment = Voucher::create($data);
                $dataResponse = new VoucherResource($payment);
                return $this->successResponse($dataResponse);

        } catch (MongoError $exception) {
            return $this->errorResponse(['Error created Payment']);
        }
    }

      public  function getMonthlyAmount($card_id)
    {
        $vouchers = Voucher::where('car_id', $card_id )->orderBy('amount', 'desc')->get();

        if (! count($vouchers) > 0 ){
            $lastAmount =  0 ;
            return $lastAmount;
        }
         $lastAmount = $vouchers->first();
        return  $lastAmount->amount;
    }
    
}
