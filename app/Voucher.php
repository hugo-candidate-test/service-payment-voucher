<?php

namespace App;
// use Illuminate\Database\Eloquent\Model;
use  \Jenssegers\Mongodb\Eloquent\Model;

class Voucher extends Model 
{

 protected $collection = 'vouchers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'time', 'status', 'id', 'amount', 'car_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
