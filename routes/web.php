<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/payments', 'PaymentsController@index');
$router->post('/payments', 'PaymentsController@store');
$router->get('/get-monthly-amount', 'PaymentsController@getMonthlyAmount');
$router->patch('/payments/{id}', 'PaymentsController@update');
// $router->get('/payments/{id}', 'PaymentsController@show');
