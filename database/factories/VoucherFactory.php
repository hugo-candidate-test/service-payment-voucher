<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Voucher::class, function (Faker\Generator $faker) {
    return [
        'start_time' => $faker->dateTime,
        'end_time' => $faker->dateTime,
        'car_id' => $faker->unique()->randomNumber($nbDigits = NULL, $strict = false),
        'type_of_car' => $faker->randomElement(['resident','official', 'no-official'])
    ];
});
